package br.com.drummond.escola.manipula;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import br.com.drummond.escola.dao.AlunoDAO;
import br.com.drummond.escola.modelo.Aluno;

public class InsertAluno {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		
		Aluno aluno = new Aluno();
		aluno.setNome("Fulano Ativo");
		aluno.setEmail("fulano@gmail.com");
		calendar.set(1980, 12, 30);
		aluno.setNascimento(calendar.getTime());
		aluno.setNascimento(new Date());
		aluno.setStatus(true);
		
		AlunoDAO dao = null;
		try {
			dao = new AlunoDAO();
			long ra = dao.insere(aluno);
			System.out.println("RA: "+ra);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				System.out.println("N�o foi poss�vel fechar a conex�o.");
			}
		}	
	}
}
