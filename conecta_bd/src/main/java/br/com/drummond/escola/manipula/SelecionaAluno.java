package br.com.drummond.escola.manipula;

import java.sql.SQLException;
import java.util.List;

import br.com.drummond.escola.dao.AlunoDAO;
import br.com.drummond.escola.modelo.Aluno;

public class SelecionaAluno {

	public static void main(String[] args) {
		AlunoDAO dao = null;
		try {
			dao = new AlunoDAO();
			List<Aluno> lista = dao.lista();
			for(int i=0;i<lista.size();i++) {
				System.out.println(lista.get(i).getNome());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
