package br.com.drummond.escola.manipula;

import java.sql.SQLException;

import br.com.drummond.escola.dao.AlunoDAO;
import br.com.drummond.escola.modelo.Aluno;

public class AtualizaAluno {

	public static void main(String[] args) {

		AlunoDAO dao = null;
		
		Aluno aluno;
		try {
			dao = new AlunoDAO();
			aluno = dao.buscaPorRa(5);
			aluno.setNome("Fulano aaaa");
			aluno.setEmail("fulano@asfadfgmail.com");
			
			dao.atualiza(aluno);
		} catch (SQLException e1) {
			System.out.println("Erro: "+e1.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				System.out.println("N�o foi poss�vel fechar a conex�o.");
			}
		}
	}
}
