package br.com.drummond.escola.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.drummond.escola.conexao.ConnectionFactory;
import br.com.drummond.escola.modelo.Aluno;

public class AlunoDAO {

	private Connection conexao = null;

	public AlunoDAO() throws ClassNotFoundException {
		conexao = new ConnectionFactory().getConnection();
	}
	
	public void close() throws SQLException {
		conexao.close();
	}

	public long insere(Aluno aluno) throws SQLException {

		long ra = 0L;

		String sql = "insert into aluno (nome, email, status) values (?, ?, ?)";
		PreparedStatement stmt = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		// preenche os valores
		stmt.setString(1, aluno.getNome());
		stmt.setString(2, aluno.getEmail());
		stmt.setBoolean(3,  true);
		stmt.execute();

		ResultSet generatedKeys = stmt.getGeneratedKeys();
		if (generatedKeys.next()) 
			ra = generatedKeys.getLong(1);

		stmt.close();

		return ra;
	}

	public void atualiza(Aluno aluno) throws SQLException {
		String sql = "update aluno set nome=?, email=? where ra=?";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		stmt.setString(1, aluno.getNome());
		stmt.setString(2, aluno.getEmail());
		stmt.setLong(3, aluno.getRa());
		stmt.execute();
		stmt.close();
	}
	
	public Aluno buscaPorRa(long ra) throws SQLException {
		PreparedStatement stmt = conexao.prepareStatement("select * from aluno where ra = ?");
		stmt.setLong(1, ra);

		ResultSet rs = stmt.executeQuery();

		Aluno aluno = new Aluno();
		while (rs.next()) {
			aluno.setRa(rs.getLong("ra"));
			aluno.setNome(rs.getString("nome"));
			aluno.setEmail(rs.getString("email"));
			aluno.setNascimento(rs.getDate("nascimento"));
		}

		rs.close();
		stmt.close();

		return aluno;
	}

	public List<Aluno> lista() throws SQLException {
		ArrayList<Aluno> alunos = new ArrayList<Aluno>();
		PreparedStatement stmt = conexao.prepareStatement("select * from aluno");

		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			Aluno aluno = new Aluno();
			aluno.setRa(rs.getLong("ra"));
			aluno.setNome(rs.getString("nome"));
			aluno.setEmail(rs.getString("email"));
			alunos.add(aluno);
		}

		rs.close();
		stmt.close();

		return alunos;
	}
	
	public void excluir(long ra) throws SQLException {
		String sql = "delete from aluno where ra = ?";
		PreparedStatement stmt = conexao.prepareStatement(sql);
		stmt.setLong(1, ra);
		stmt.executeUpdate();
		stmt.close();
	}
}
