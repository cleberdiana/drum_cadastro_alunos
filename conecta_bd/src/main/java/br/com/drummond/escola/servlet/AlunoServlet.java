package br.com.drummond.escola.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.drummond.escola.dao.AlunoDAO;
import br.com.drummond.escola.modelo.Aluno;

@WebServlet(urlPatterns={"/aluno"})
public class AlunoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String menu = req.getParameter("mn");
		
		if (menu == null || menu.isEmpty()) {
			req.getRequestDispatcher("index.jsp").forward(req, resp);
		} else {
			switch (menu) {
				case "lista":
					lista(req, resp);
					break;
				case "salvar":
					salvar(req, resp);
					break;
				case "novo":
					req.getRequestDispatcher("cadastro.jsp").forward(req, resp);
					break;
				case "editar":
					editar(req, resp);
					break;
				case "delete":
					excluir(req, resp);
					break;
				default:
					req.getRequestDispatcher("index.jsp").forward(req, resp);
					break;
			}
		}
	}

	private void editar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String mensagem = "";
		Long ra = Long.valueOf(req.getParameter("ra"));
		
		Aluno aluno = null;
		AlunoDAO dao = null;
		try {
			// instancia o dao.
			dao = new AlunoDAO();
			aluno = dao.buscaPorRa(ra);
		} catch (ClassNotFoundException e) {
			mensagem = "Houve um erro ao acessar o banco de dados: "+e.getMessage()+"<br>";
		} catch (SQLException e) {
			mensagem = "Houve um erro ao tentar excluir o registro: "+e.getMessage()+"<br>";
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				mensagem = "Houve um erro ao fechar a conex�o com o banco de dados: "+e.getMessage();
			}
		}
		req.setAttribute("aluno", aluno);
		req.setAttribute("mensagem", mensagem);
		req.getRequestDispatcher("cadastro.jsp").forward(req, resp);		
	}

	private void excluir(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String mensagem = "";
		Long ra = Long.valueOf(req.getParameter("ra"));
		
		AlunoDAO dao = null;
		try {
			// instancia o dao.
			dao = new AlunoDAO();

			// realiza a exclusao do aluno no banco.
			dao.excluir(ra);
			
			mensagem = "O Aluno com R.A.: "+ra+" foi excluido com sucesso!<br>";
			
		} catch (ClassNotFoundException e) {
			mensagem = "Houve um erro ao acessar o banco de dados: "+e.getMessage()+"<br>";
		} catch (SQLException e) {
			mensagem = "Houve um erro ao tentar excluir o registro: "+e.getMessage()+"<br>";
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				mensagem = "Houve um erro ao fechar a conex�o com o banco de dados: "+e.getMessage();
			}
		}
		
		// caso houver erro vai preenchido na variavel.
		req.setAttribute("mensagem", mensagem);
		
		// envia para a tela lista.
		req.getRequestDispatcher("aluno?mn=lista").forward(req, resp);
	}

	private void salvar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Long ra = Long.valueOf(req.getParameter("ra"));
		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		
		// cria o objeto aluno.
		// preenche com os dados vindos do formul�rio.
		Aluno aluno = new Aluno();
		aluno.setNome(nome);
		aluno.setEmail(email);
				
		String erro = "";
		
		// valida os dados de entrada.
		if (nome.equals(null) || nome.isEmpty()) erro += "Nome n�o pode estar vazio.<br>";
		if (email.equals(null) || email.isEmpty()) erro += "E-mail n�o pode estar vazio.<br>";
		
		if (!erro.isEmpty()) {
			req.setAttribute("erro", erro);
			req.setAttribute("aluno", aluno);
			req.getRequestDispatcher("cadastro.jsp").forward(req, resp);
		} else {
		
			AlunoDAO dao = null;
			try {
				// instancia o dao.
				dao = new AlunoDAO();				

				if (ra != null) {
					aluno.setRa(ra);
					dao.atualiza(aluno);
				} else {
					// realiza a insercao do aluno no banco.
					dao.insere(aluno);
				}
				
				if (resp.isCommitted())
					resp.sendRedirect("aluno?mn=lista");
				
			} catch (ClassNotFoundException e) {
				erro = "Houve um erro ao acessar o banco de dados: "+e.getMessage()+"<br>";
			} catch (SQLException e) {
				erro = "Houve um erro ao tentar excluir o registro: "+e.getMessage()+"<br>";
			} finally {
				try {
					dao.close();
				} catch (SQLException e) {
					e.printStackTrace();	// erro caso nao consiga fechar a conexao.
				}
			}
			
			// caso houver erro vai preenchido na variavel.
			req.setAttribute("erro", erro);
			
			// envia para a tela lista.
			req.getRequestDispatcher("aluno?mn=lista").forward(req, resp);
		}
	}

	private void lista(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AlunoDAO dao = null;
		List<Aluno> lista = null;
		try {
			dao = new AlunoDAO();
			lista = dao.lista();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		req.setAttribute("lista", lista);
		RequestDispatcher dispatcher = req.getRequestDispatcher("aluno_lista.jsp");
		dispatcher.forward(req, resp);
	}
	
	
}
