package br.com.drummond.escola.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCConexao {

	public static void main(String[] args) {
		System.out.println("Conectado!");
		try {
			Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost/drum_conecta", "root", "");
			
			String sql = "insert into aluno (nome,email,nascimento) values (?,?,?)";
	        PreparedStatement stmt = conexao.prepareStatement(sql);

	        // preenche os valores
	        stmt.setString(1, "Caelum");
	        stmt.setString(2, "contato@caelum.com.br");
	        stmt.setString(3, "1973-01-19");

	        // executa
	        stmt.execute();
	        stmt.close();

	        System.out.println("Gravado!");
	        
			conexao.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
