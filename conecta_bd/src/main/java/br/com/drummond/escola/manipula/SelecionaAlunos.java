package br.com.drummond.escola.manipula;

import java.sql.SQLException;
import java.util.List;

import br.com.drummond.escola.dao.AlunoDAO;
import br.com.drummond.escola.modelo.Aluno;

public class SelecionaAlunos {

	public static void main(String[] args) {
		AlunoDAO dao = null;
		try {
			dao = new AlunoDAO();
			List<Aluno> lista = dao.lista();
			for (Aluno aluno : lista) {
				System.out.println("ra: "+aluno.getRa()+" Nome: "+aluno.getNome());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				dao.close();
			} catch (SQLException e) {
				System.out.println("N�o foi poss�vel fechar a conex�o.");
			}
		}
	}
}
