<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<jsp:include page="cabecalho.jsp" />

<div class="container">

	<h3>Cadastro de Alunos</h3>
	
	<c:if test="${not empty erro}">
		<div class="alert alert-danger alert-dismissible" role="alert">
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  				<span aria-hidden="true">&times;</span>
  			</button>
			${erro}
		</div>
	</c:if>
	
	<form method="get" action="aluno">
		<div class="col-md-6">
			<input type="hidden" name="mn" value="salvar" />
			<input type="hidden" name="ra" value="${aluno.ra}" />
			<div class="form-group">
				<label for="nome">Nome</label> 
				<input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do aluno" value="${aluno.nome}" />
			</div>
			<div class="form-group">
				<label for="email">E-mail</label> 
				<input type="email" class="form-control" id="email" name="email" placeholder="E-mail do aluno" value="${aluno.email}" />
			</div>
			<button type="submit" class="btn btn-default">Cadastrar</button>
		</div>
	</form>
</div>

<jsp:include page="rodape.jsp" />
