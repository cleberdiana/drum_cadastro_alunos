<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="cabecalho.jsp" />

<div class="container">
	<h3>Lista de Alunos</h3>
	
	<c:if test="${not empty mensagem}">
		<div class="alert alert-warning alert-dismissible" role="alert">
  			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  				<span aria-hidden="true">&times;</span>
  			</button>
			${mensagem}
		</div>
	</c:if>
	
	<table class="table table-hover table-bordered table-condensed">
	<tr>
		<th>R.A.</th>
		<th>Nome</th>
		<th>E-mail</th>
		<th width="5"></th>
		<th width="5"></th>
	</tr>
		<c:forEach items="${lista}" var="v">
			<tr>
				<td>${v.ra}</td>
				<td>${v.nome}</td>
				<td>${v.email}</td>
				<td>
					<a href="aluno?mn=editar&ra=${v.ra}">
						<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
					</a>				
				</td>
				<td>
					<a href="aluno?mn=delete&ra=${v.ra}">
						<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
					</a>				
				</td>
			</tr>
		</c:forEach>
	</table>
</div>

<jsp:include page="rodape.jsp" />

